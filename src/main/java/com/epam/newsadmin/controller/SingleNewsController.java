package com.epam.newsadmin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsadmin.exception.ControllerException;
import com.epam.newscommon.entity.CommentEntity;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.ICommentService;
import com.epam.newscommon.service.INewsManagementService;
import com.epam.newscommon.service.INewsService;
import com.epam.newscommon.valueobject.NewsSearchCriteria;
import com.epam.newscommon.valueobject.NewsVO;

@Controller
@SessionAttributes(value = { "filteredItem" })
public class SingleNewsController {

	@Autowired
	private INewsManagementService newsManageService;

	@Autowired
	private INewsService newsService;

	@Autowired
	private ICommentService commentService;

	@RequestMapping(value = "/news/next/{newsId}", method = RequestMethod.GET)
	public String loadNextNews(@ModelAttribute("filteredItem") NewsSearchCriteria filteredItem,
			@PathVariable Long newsId) throws ControllerException {
		try {
			Long nextId = newsService.loadNextId(filteredItem, newsId);
			if (nextId == null) {
				nextId = newsId;
			}
			return "redirect:/news/" + nextId;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	@RequestMapping(value = "/news/previous/{newsId}", method = RequestMethod.GET)
	public String loadPreviousNews(@ModelAttribute("filteredItem") NewsSearchCriteria filteredItem,
			@PathVariable Long newsId) throws ControllerException {
		try {
			Long previousId = newsService.loadPreviousId(filteredItem, newsId);
			if (previousId == null) {
				previousId = newsId;
			}
			return "redirect:/news/" + previousId;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.GET)
	public ModelAndView loadById(@PathVariable Long newsId,
			@ModelAttribute("filteredItem") NewsSearchCriteria filteredItem) throws ControllerException {
		try {
			ModelAndView model = new ModelAndView("news/item");
			NewsVO newsObject = newsManageService.loadById(newsId);
			model.addObject("newsObject", newsObject);
			model.addObject("comment", new CommentEntity());
			Long nextId = newsService.loadNextId(filteredItem, newsId);
			Long previousId = newsService.loadPreviousId(filteredItem, newsId);
			model.addObject("nextId", nextId);
			model.addObject("previousId", previousId);
			return model;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	@RequestMapping(value = "/comment/create", method = RequestMethod.POST)
	public String createComment(@ModelAttribute("comment") CommentEntity entity) throws ControllerException {
		try {
			commentService.create(entity);
			return "redirect:/news/" + entity.getNewsId();
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	@RequestMapping(value = "/comment/delete")
	public String delete(@RequestParam("commentId") Long commentId, @RequestParam("newsId") Long newsId)
			throws ControllerException {
		try {
			commentService.delete(commentId);
			return "redirect:/news/" + newsId;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}
}
