package com.epam.newsadmin.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.epam.newsadmin.exception.ControllerException;
import com.epam.newscommon.entity.AuthorEntity;
import com.epam.newscommon.entity.NewsEntity;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.IAuthorService;
import com.epam.newscommon.service.INewsManagementService;
import com.epam.newscommon.service.ITagService;
import com.epam.newscommon.valueobject.NewsVO;

@Controller
public class CreateUpdateNewsController {
	@Autowired
	private INewsManagementService newsManageService;
	@Autowired
	private ITagService tagService;
	@Autowired
	private IAuthorService authorService;
	@Autowired
	private MessageSource messageSource;

	@InitBinder
	public void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		Locale locale = RequestContextUtils.getLocale(request);
		SimpleDateFormat dateFormat = null;
		if (locale.getLanguage().equals("ru")) {
			dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		} else {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		}
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, null, new CustomDateEditor(
				dateFormat, true));
	}

	@RequestMapping(value = "/news/create", method = RequestMethod.GET)
	public ModelAndView showFormToCreate() throws ControllerException {
		try {
			ModelAndView model = new ModelAndView("news/create");
			model.addObject("newsEntity", new NewsEntity());
			model.addObject("tagList", tagService.loadAll());
			model.addObject("authorList", authorService.loadActiveAuthors());
			return model;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	@RequestMapping(value = "/news/create", method = RequestMethod.POST)
	public String create(
			@ModelAttribute("newsEntity") NewsEntity newsEntity,
			@RequestParam(value = "tagId", required = false) List<Long> tagIdList,
			@RequestParam("authorId") Long authorId, RedirectAttributes ra,
			HttpServletRequest request) throws ControllerException {
		try {
			Long newsId = newsManageService.create(newsEntity, tagIdList,
					authorId);
			Locale locale = RequestContextUtils.getLocale(request);
			ra.addFlashAttribute("successMessage",
					messageSource.getMessage("message.news.add", null, locale));
			return "redirect:/news/" + newsId;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	@RequestMapping(value = "/news/update/{newsId}", method = RequestMethod.GET)
	public ModelAndView showFormToUpdate(@PathVariable("newsId") Long id)
			throws ControllerException {
		try {
			ModelAndView model = new ModelAndView("news/create");
			NewsVO newsObject = newsManageService.loadById(id);
			model.addObject("newsObject", newsObject);
			model.addObject("tagList", tagService.loadAll());
			List<AuthorEntity> authorList = authorService.loadActiveAuthors();
			AuthorEntity authorEntity = newsObject.getAuthorEntity();
			if (authorEntity.getExpiredDate() != null) {
				authorList.add(authorEntity);
			}
			model.addObject("authorList", authorList);
			return model;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	@RequestMapping(value = "/news/update", method = RequestMethod.POST)
	public String update(
			@ModelAttribute("newsEntity") NewsEntity newsEntity,
			@RequestParam(value = "tagId", required = false) List<Long> tagIdList,
			@RequestParam("authorId") Long authorId, RedirectAttributes ra,
			HttpServletRequest request) throws ServiceException {
		newsManageService.update(newsEntity, tagIdList, authorId);

		Locale locale = RequestContextUtils.getLocale(request);
		ra.addFlashAttribute("successMessage",
				messageSource.getMessage("message.news.update", null, locale));
		return "redirect:/news/page/1";

	}
}
